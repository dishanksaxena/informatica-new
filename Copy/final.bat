:loop
cd C:\Informatica\9.1.0\clients\PowerCenterClient\client\bin
pmrep connect -r informatica_poc -d Domain_EC2AMAZ-DUFAH6M -n Administrator -x Administrator

cd C:\Informatica\9.1.0\clients\PowerCenterClient\client\bin
pmrep objectexport -n m_medicine_scd2_flat_file -o mapping -s -b -r -f folder -u "C:\Users\Administrator\Desktop\Copy\mymap.xml"

cd C:\Users\Administrator\Desktop\Copy
PowerShell.exe -ExecutionPolicy Bypass -Command "get-content mymap.xml | Select -Index 101" > m_medicine_scd2_flat_file_without_Upper_case.xml

fc /c "C:\Users\Administrator\Desktop\Copy\m_medicine_scd2_flat_file_without_Upper_case.xml" "C:\Users\Administrator\Desktop\Copy\same\m_medicine_scd2_flat_file_without_Upper_case.xml" > "C:\Users\Administrator\Desktop\Copy\compare.txt"

cd "C:\Users\Administrator\Desktop\Copy"
findstr "no differences encountered" compare.txt>NUL
IF ERRORLEVEL 1 (git add .) ELSE (goto loop)
findstr "no differences encountered" compare.txt>NUL
IF ERRORLEVEL 1 (git commit -m "commit") ELSE (exit 0)
findstr "no differences encountered" compare.txt>NUL
IF ERRORLEVEL 1 (git push -u origin master) ELSE (exit 0)
findstr "no differences encountered" compare.txt>NUL
IF ERRORLEVEL 1 (call second.bat) ELSE (exit 0)